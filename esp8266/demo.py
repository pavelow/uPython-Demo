#ESP8266 Demo
import network
import socket
from simple import MQTTClient
import ubinascii
import machine
from machine import Pin
import time

SERVER = "192.168.4.2"
CLIENT_ID = ubinascii.hexlify(machine.unique_id())
TOPICB = b"Button"
TOPICADC = b"ADC"
btn = 0

def initWifi():
    ap = network.WLAN(network.STA_IF)
    ap.active(True)
    ap.connect('uPy-DEMO','password')
    return ap

def mqtt(server=SERVER):
    adc = machine.ADC(0)
    button = Pin(5, Pin.IN, Pin.PULL_UP)
    c = MQTTClient(CLIENT_ID, server)
    c.set_callback(sub_cb)
    c.connect()
    print("MQTT connected")
    c.subscribe(b"LED")
    timelast = 0
    while True:
        if button.value() == 0:
            if(btn == 0):
                c.publish(TOPICB,b"1")
            btn = 1
        else:
            btn = 0

        if(time.ticks_ms() > timelast):
            c.publish(TOPICADC, str(adc.read()))
            timelast = time.ticks_ms() + 200
        #time.sleep_ms(20)
        c.check_msg()
    c.disconnect()

def sub_cb(topic, msg):
    if(topic == b"LED"):
        led = Pin(4, Pin.OUT)
        if(msg == b"1"):
            led.value(1)
        else:
            led.value(0)

def http():
    addr = socket.getaddrinfo('0.0.0.0',80)[0][-1]
    s = socket.socket()
    s.bind(addr)
    s.listen(1)
    print("Listening on", addr)
    while True:
        cl, addr = s.accept()
        print("Connection from",addr)
        response = "Hello from ESP8266\n"
        cl.send(response)
        cl.close()
