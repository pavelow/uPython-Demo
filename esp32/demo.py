import network
from simple import MQTTClient
import ubinascii
import machine
import time

SERVER = "192.168.4.2"
CLIENT_ID = ubinascii.hexlify(machine.unique_id())
TOPIC = b"ESP32"

def initWifi():
    ap = network.WLAN(network.AP_IF)
    ap.active(True)
    ap.config(essid='uPy-DEMO',authmode=network.AUTH_WPA2_PSK,password='password')
    return ap

def main(server=SERVER):
    c = MQTTClient(CLIENT_ID, server)
    c.connect()
    print("MQTT connected")
    while True:
        c.publish(TOPIC,b"500")
        time.sleep_ms(500)

    c.disconnect()
