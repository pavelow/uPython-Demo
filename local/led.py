#! /usr/bin/python
import paho.mqtt.client as mqtt
import sys

client = mqtt.Client()
client.connect("127.0.0.1")

if len(sys.argv) > 1:
    client.publish("LED",sys.argv[1])
else:
    print("Need an argument [1|0]")
