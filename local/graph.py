#! /usr/bin/python
#Graph Data as from ESP8266 ADC via mqtt
import matplotlib.pyplot as plt
from drawnow import *
import paho.mqtt.client as mqtt

def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT server")
    client.subscribe("ADC")

def on_message(client, userdata, msg):
    Voltage = float(msg.payload) * (3.3/1024) #Conversion
    values.append(Voltage)
    values.pop(0)
    drawnow(plotValues)

def plotValues():
    plt.ylim(0,3.4)
    plt.title('ESP8266 ADC')
    plt.grid(True)
    plt.ylabel('Voltage')
    plt.plot(values, 'r-', label='ADC0')
    plt.legend(loc='upper right')

def doAtExit():
    print("Close")
values = []

for i in range(0,100): #pre-load dummy data
    values.append(0)

plt.ion()
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("127.0.0.1")
client.loop_forever()
