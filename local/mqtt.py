#! /usr/bin/python
import paho.mqtt.client as mqtt

channels = ["ESP32", "Button", "LED"]

def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT server")
    for channel in channels:
        client.subscribe(channel)
        print("Subscribed to {}".format(channel))

def on_message(client, userdata, msg):
    print(msg.topic+ " "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("127.0.0.1")

client.loop_forever()
